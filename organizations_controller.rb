# /app/controllers/organizations_controller.rb

class OrganizationsController < ApplicationController

  def profile
    @organization = OrganizationPresenter.present(@organization, view_context)
    @projects = @organization.projects
    @projects_count = @projects.count
    @organization_members_count = @organization.members.count
    @user = current_user
    set_variables_for_content_page if params[:page]
    @params_page = params[:page].presence
  end
end
