# /app/presenters/organization_presenter.rb

class OrganizationPresenter < UPresenter::Base
  def change_organization_profile_link(current_user)
    return unless current_user.organization_owner?(current_user.id, self.id)
    view_context.link_to('Change',
                         view_context.profile_content_path(self, :edit),
                         remote: true,
                         class: 'edit_profile')
  end

  def add_organization_members_link(current_user)
    return unless current_user.organization_owner?(current_user.id, self.id)
    view_context.link_to(I18n.t('add_members'),
                         '#',
                         class: 'add-organization-member')
  end

  def left_menu(current_user)
    left_menu_links = {
      organization_profiler: I18n.t('organization_profile'),
      projects: I18n.t('projects'),
      members: I18n.t('organization_members'),
      plans_and_billings: I18n.t('plans_and_billings'),
      payment_history: I18n.t('payment_history')
    }
    return left_menu_links unless current_user.organization_owner?(current_user.id,
                                                                   self.id)

    left_menu_links.merge!({ delete_organization: I18n.t('delete_organization') })
  end

  def active_page(params_page, left_menu_links)
    page = params_page || ''
    pages = left_menu_links.keys << :edit
    return 'organization_profiler' unless pages.include?(page.to_sym)
    params_page || 'organization_profiler'
  end
end
